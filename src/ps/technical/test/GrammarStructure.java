/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ps.technical.test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jzapatam
 */
public class GrammarStructure {
    
    private Path filePath;
    private final static Charset ENCODING = StandardCharsets.UTF_8;
    private final static String RULE_DELIM=":";
    private final static String RULE_DEF_DELIM=" ";
    private final static String RULE_CONTENT_DELIM="|";
    public final  String END_DIRECTIVE="$END";
    public final  String LINEBREAK_DIRECTIVE="$LINEBREAK";
    public HashMap grammarStructure=new HashMap();
    
    
    public GrammarStructure(String file){
        try{
            filePath=Paths.get(file);
            
        }catch(FileSystemNotFoundException ex){
            System.out.println("The file you provided does not exist: "+ex.getMessage());
        }
    }
    
    public void build(){
        try{
            //read file line by line
            Scanner scanFile =  new Scanner(filePath);
            
            while (scanFile.hasNextLine()){
                processRule(scanFile.nextLine());
            }
            System.out.println("The grammar structure has been successfully created");
        }catch(IOException ex){
            System.out.println("ERROR: "+ex.getMessage());
        }
       
    }
    
    private void processRule(String ruleLine){
        StringTokenizer parserRule = new StringTokenizer(ruleLine,RULE_DELIM);
        
        
        String ruleName;
        String ruleDefinition;
        ArrayList<String> contentRule;
        StringTokenizer parserRuleDef;
        StringTokenizer parserRuleContent;
        
        String currentToken;
        String contentName;
        String currentKeyWord;
        boolean hasKeyWord;
        int sizeContentArray;
        
        
        if(parserRule.countTokens()==2){
            while(parserRule.hasMoreTokens()){
                
                ruleName=parserRule.nextToken();
                ruleDefinition="";
                
                parserRuleDef=new StringTokenizer(parserRule.nextToken().trim(),RULE_DEF_DELIM);
                int i=1;
                while(parserRuleDef.hasMoreTokens()){
                    
                    currentKeyWord="";
                    hasKeyWord=false;
                    sizeContentArray=0;
                    
                    currentToken=parserRuleDef.nextToken().trim();
                    parserRuleContent=new StringTokenizer(currentToken,RULE_CONTENT_DELIM);
                   
                    if(parserRuleContent.countTokens()==1){
                                                
                        if(ruleDefinition.equals("")){
                            ruleDefinition+=currentToken;
                        }else{
                            ruleDefinition+="&"+currentToken;
                        }
                        
                    }else{
                        
                        contentName="<"+ruleName+"_CONTENT_"+i+">";
                        contentRule=new ArrayList();
                        while(parserRuleContent.hasMoreTokens()){
                            currentToken=parserRuleContent.nextToken();
                            contentRule.add(currentToken);
                            sizeContentArray++;
                            if(isKeyWord(currentToken)){
                                currentKeyWord=currentToken;
                                hasKeyWord=true;
                            }
                        }
                        
                        if(hasKeyWord){
                            int index=0;
                            while(index<sizeContentArray-2){
                                contentRule.add(currentKeyWord);
                                index++;
                            }
                            Collections.shuffle(contentRule);
                        }
                        
                        this.grammarStructure.put(contentName, contentRule);
                        
                        
                        if(ruleDefinition.equals("")){
                            ruleDefinition+=contentName;
                        }else{
                            ruleDefinition+="&"+contentName;
                        }
                    }
                    
                    i++;
                }
                if(!isRule(ruleName)){
                    ruleName="<"+ruleName+">";
                }
                this.grammarStructure.put(ruleName, ruleDefinition);
                
            }
            
        }else{
            System.out.println("Invalid rule structure for row '"+ruleLine+"'");
        }
    }
    
    public boolean isRule(String token){
        Pattern regEx = Pattern.compile("<([a-zA-Z]\\w*?)>");
        Matcher match=regEx.matcher(token);
        return match.matches();
    }
    
    public boolean isKeyWord(String token){
        return token.equals(END_DIRECTIVE)||token.equals(LINEBREAK_DIRECTIVE);
    }
    
}
