/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ps.technical.test;

import java.util.Scanner;

/**
 *
 * @author jzapatam
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            String file;

            if(args.length == 0){
                
                System.out.println("Please insert the path and name of grammatical rules file:");

                Scanner reader= new Scanner(System.in);
                file=reader.nextLine();

            }else{
                file=args[0];
            }
            
            TextGenerator textGen = new TextGenerator();
            textGen.generate(file);
            
            
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println(ex.getMessage());
        }
    }
    
}
