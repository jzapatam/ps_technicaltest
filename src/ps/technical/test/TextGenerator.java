/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ps.technical.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author jzapata
 */
public class TextGenerator {
    
    private String ruleText="";
    GrammarStructure grammar;
    private boolean genText;
    private ArrayList<String> rulesValidated=new ArrayList();;
    //private Set gramStructure;
    
    public void generate(String file){
         grammar = new GrammarStructure(file);
         grammar.build();
         
         //gramStructure=grammar.grammarStructure.entrySet();
         Set<String> keysGrammar = grammar.grammarStructure.keySet();
         
         String validEntryWords="";
         String key;
         Iterator<String> keysIterator=keysGrammar.iterator();
         while(keysIterator.hasNext()){
             key=keysIterator.next();
             
             if(grammar.grammarStructure.get(key).getClass().getSimpleName().equals("String")){
                if(validEntryWords.equals("")){
                    validEntryWords=key;
                }else{
                    validEntryWords+=", "+key;
                }
             }
         }
         validEntryWords=validEntryWords.replaceAll("<", "").replaceAll(">", "");
         System.out.println("Now, enter the rule that you want to generate (valid values: "+validEntryWords+")");
         
         Scanner reader= new Scanner(System.in);
         String rule="<"+reader.nextLine()+">";
         
         if(keysGrammar.contains(rule)){
             System.out.println("Generating "+rule+"....");
             rulesValidated.clear();
             if(validateDirective(rule,grammar.END_DIRECTIVE)){
                //rulesValidated.clear();
                //if(validateDirective(rule,grammar.LINEBREAK_DIRECTIVE)){
                    this.genText=true;
                    generateRuleText(rule);
                    
                    if(this.genText){
                        System.out.println("The text for rule '"+rule+"' has been succesfully generated:");
                        System.out.println(this.ruleText.trim());
                    }
                //}else{
                //    System.out.println("LineBreak directive doesn't found for rule '"+rule+"', the program must be terminated");
                //}
             }else{
                System.out.println("End directive doesn't found for rule '"+rule+"', the program must be terminated");
             }
                
             
         }else{
             System.out.println("You entered the name of a rule that does not exist");
         }
    }
    
    private void generateRuleText(String rule){
        //validating that the grammar has an END Directive
        try{
            
            boolean isRule=grammar.isRule(rule);
            boolean isKeyWords=grammar.isKeyWord(rule);

            /*if(isRule){
                rule=rule.replace("<", "").replace(">", "");
            }*/
                
            if(isKeyWords){
                if(rule.equals(grammar.LINEBREAK_DIRECTIVE)){
                    this.ruleText+="\n";
                }
                this.genText&=true;
            }else{
                if(grammar.grammarStructure.containsKey(rule)){
                    Object element = grammar.grammarStructure.get(rule);
                    List<String> gramElements;

                    if(element.getClass().getSimpleName().equals("String")){
                        gramElements =  Arrays.asList(element.toString().split("&"));
                        Iterator<String> iteratorElements = gramElements.iterator();
                        while(iteratorElements.hasNext()){
                            generateRuleText(iteratorElements.next());
                            
                        }
                    }else{
                        gramElements = (ArrayList<String>)element;
                        Random randomGenerator=new Random();
                        int index = randomGenerator.nextInt(gramElements.size());
                        generateRuleText(gramElements.get(index));
                    }    
                   
                }else{
                    //if(this.ruleText.equals("")){
                        //this.ruleText=rule;
                    //}else{
                        this.ruleText+=rule+" ";
                    //}
                }
            }
            this.genText&= true;
        }catch(Exception ex){
            System.out.println("ERROR: "+ex.getMessage());
            this.genText&= false;
        }
    }

    private boolean validateDirective(String rule, String directive) {
        try{
            if(!rulesValidated.contains(rule)){
                String currentToken="";
                /*if(grammar.isRule(rule)){
                    rule=rule.replace("<", "").replace(">", "");
                }*/
                if(grammar.grammarStructure.containsKey(rule)){
                    rulesValidated.add(rule);
                    Object element = grammar.grammarStructure.get(rule);
                    List<String> gramElements;

                    if(element.getClass().getSimpleName().equals("String")){
                        gramElements =  Arrays.asList(element.toString().split("&"));
                    }else{
                        gramElements = (ArrayList<String>)element;
                    }    

                    if(gramElements.contains(directive)){
                        return true;
                    }else{
                        Iterator<String> iteratorElements = gramElements.iterator();
                        while(iteratorElements.hasNext()){
                            currentToken=iteratorElements.next();
                            /*if(grammar.isRule(currentToken)){
                                currentToken=currentToken.replace("<", "").replace(">", "");
                            }*/
                            if(grammar.grammarStructure.containsKey(currentToken)){
                                if(validateDirective(currentToken,directive)){
                                    return true;
                                }
                            }
                        }
                    }
                }
            }    
            return false;
        }catch(Exception ex){
            System.out.println("ERROR: "+ex.getMessage());
            return false;
        }
    }
    
}
